import numpy as np

pieces = [2,1,1,1,2,1,2,1,1,1,1,2,1,2,1,1,1,1,1,2]

cube = np.zeros((3,3,3))

class State:
	def __init__(self, cube, pos, direction, piece_i):
		self.cube = cube
		self.pos = pos
		self.direction = direction
		self.piece_i = piece_i


	def fits(self):
		length = pieces[self.piece_i]		
		
		for i in range(1,length+1):
			if self.pos[0]+self.direction[0]*i < 0 or self.pos[0]+self.direction[0]*i >= 3:
				return False
			elif self.pos[1] + self.direction[1]*i < 0 or self.pos[1] + self.direction[1]*i >= 3:
				return False
			elif self.pos[2] + self.direction[2]*i < 0 or self.pos[2] + self.direction[2]*i >= 3:
				return False
			if self.cube[self.pos[0]+self.direction[0]*i, self.pos[1] + self.direction[1]*i, self.pos[2] + self.direction[2]*i]:
				return False
		return True
	def get_cubes_after_fill(self):
		if not self.fits():
			return []
		else:
			length = pieces[self.piece_i]
			new_cube = np.zeros((3,3,3))
			
			for i in range(3):
				for j in range(3):
					for k in range(3):
						new_cube[i,j,k] = self.cube[i,j,k]
			for i in range(1,length+1):
				new_cube[self.pos[0]+self.direction[0]*i,self.pos[1]+self.direction[1]*i,self.pos[2]+self.direction[2]*i] = True
			new_pos = (self.pos[0]+self.direction[0]*length,self.pos[1]+self.direction[1]*length,self.pos[2]+self.direction[2]*length)
			new_states = []
			new_states.append(State(new_cube, new_pos, (self.direction[1], self.direction[2], self.direction[0]), self.piece_i+1))
			new_states.append(State(new_cube, new_pos, (-self.direction[1], -self.direction[2], -self.direction[0]), self.piece_i+1))
			new_states.append(State(new_cube, new_pos, (self.direction[2], self.direction[0], self.direction[1]), self.piece_i+1))
			new_states.append(State(new_cube, new_pos, (-self.direction[2], -self.direction[0], -self.direction[1]), self.piece_i+1))
			return new_states

states = [
	State(np.zeros((3,3,3)), (0,0,0), (0,1,0), 0),
	State(np.zeros((3,3,3)), (1,0,0), (0,1,0), 0),
	State(np.zeros((3,3,3)), (0,0,1), (0,1,0), 0),
	State(np.zeros((3,3,3)), (1,0,1), (0,1,0), 0)
]

states[0].cube[0,0,0] = True
states[1].cube[1,0,0] = True
states[2].cube[0,0,1] = True
states[3].cube[1,0,1] = True

solution = {}

while len(states) > 0:
	current_state = states.pop()
	
	solution[current_state.piece_i] = current_state

	if current_state.piece_i == len(pieces):
		print("solution found!")
		break
	else:
		for s in current_state.get_cubes_after_fill():
			states.append(s)
	


for i in range(21):
	current_state = solution[i]
	print(current_state.cube)
	print("piece:", current_state.piece_i)	
	print("pos: ", current_state.pos)
	print("dir: ", current_state.direction)
	print("--------------------------------------")


